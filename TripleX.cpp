#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <time.h>




void PrintIntro(int Difficulty)
{
    std::cout << "\n********************************************************************************\n********************************************************************************" << std::endl;
    std::cout << "Your name is Jenna and you want to hack into someones computer to blackmail them" << std::endl;
    std::cout << "\nThe Difficulty of this firewall is " << Difficulty << std::endl;
    std::cout << "\nEnter the correct passcode to continue\n";
}
bool PlayGame(int Difficulty)
{
    PrintIntro(Difficulty);
    srand (static_cast<unsigned int> (time(0)));
    const int FirstNum = rand() % Difficulty + 1;
    const int SecondNum = rand() % Difficulty + 2;
    const int ThirdNum = rand() % Difficulty + 3;

    const int NumSum = FirstNum + SecondNum + ThirdNum;
    const int NumProduct = FirstNum * SecondNum * ThirdNum;

    std::cout << "\n+ There are 3 numbers in the passcode";
    std::cout << "\n+ The numbers add up to " << NumSum;
    std::cout << "\n+ The numbers multiplied are "<< NumProduct;
    
    int Guess1, Guess2, Guess3;
    std::cout << "\n\nPlease enter your Guess using a space after each number: ";
    std::cin >> Guess1 >> Guess2 >> Guess3;
   
    
    int GuessSum = Guess1 + Guess2 + Guess3;
    int GuessProduct = Guess1 * Guess2 * Guess3;

    std::cout << "\nThe numbers you guessed add up to: " << GuessSum;
    std::cout << "\nThe numbers you guessed multiply to: " << GuessProduct;

    if (GuessProduct == NumProduct && GuessSum == NumSum)
        {
            std::cout << "\n\n**Congratulations you've cracked the code you're past the level**" << std::endl;
            ++Difficulty;
            return true;
        }
    
    else
    {
        std::cout << "\n\nYou ducked up try again"<< std::endl;
        return false;
    }
}
int main()
{ 
    int LevelDifficulty = 1;
    int MaxDifficulty = 10;

    while (LevelDifficulty <= MaxDifficulty)
    {
    bool bLevelComplete = PlayGame(LevelDifficulty);
    std::cin.clear(); //ignores errors
    std::cin.ignore(); //discards buffer

    if (bLevelComplete)
    {
        ++LevelDifficulty;
    }
    else if (LevelDifficulty >= 10)
    {
        std::cout << "\n\nCongratulations you got through the firewall and got the files! now black mail that MF!!" <<std::endl;
    }

    }
    return 0;
}